{
  inputs = {
    nixpkgs.url = "github:ckiee/nixpkgs/kag-unbork";
    #nixpkgs.url = "github:NixOS/nixpkgs/2043dbb6faa9e21b0fb500161542e30d6c8bc680";
    flake-utils.url = "github:numtide/flake-utils";
    mynur.url = "github:Lykos153/nur-packages";
    mynur.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils, mynur }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ mynur.overlay ];
        };
        #mywebrtcvad = pkgs.callPackage ./webrtcvad.nix { with pkgs.python3Packages; };
        
        my-python = pkgs.python3;
        python-with-my-packages = my-python.withPackages (p: with p; [
          dragonfly
          kaldi-active-grammar
          pkgs.lykos153.python3Packages.g2p-en
          six
          sounddevice
          webrtcvad
          setuptools

          scipy
          ipython
          virtualenv

          #whisper
          pyaudio
          openai-whisper
          torch

          pip
        ]);
        kaldi-model-daanzu-smalllm = pkgs.fetchzip
          {
            url = https://github.com/daanzu/kaldi-active-grammar/releases/download/v3.0.0/kaldi_model_daanzu_20211030-smalllm.zip;
            hash = "sha256-qIqzGR4La78oPaVd6fqARgh8oH6ozzBLc1tcG2AVILU=";
          };
        kaldi-model-daanzu-mediumlm = pkgs.fetchzip
          {
            url = https://github.com/daanzu/kaldi-active-grammar/releases/download/v3.0.0/kaldi_model_daanzu_20211030-mediumlm.zip;
            hash = "sha256-PjBBW81W5uumpkxnhn7DAV2A0yemGoAW0Z4M5D+P5E8=";
          };
        kaldi-model-daanzu-biglm = pkgs.fetchzip
          {
            url = https://github.com/daanzu/kaldi-active-grammar/releases/download/v3.0.0/kaldi_model_daanzu_20211030-biglm.zip;
            hash = "sha256-5AIir7D7/krNpvR3atCgUZgM6HwPk7CXiTtym4BjxsQ=";
          };

      in {
        packages = pkgs;
        devShell = pkgs.mkShell {
         packages = [
           python-with-my-packages
         ];
         shellHook =
           ''
             (
                local model_path
                model_path="$PWD/kaldi-grammar-simple/kaldi_model"
                mkdir -p "$model_path" &&
                cp -r --reflink=always ${kaldi-model-daanzu-biglm}/* "$model_path"
                chmod -R +w "$model_path"
              )
           '';
        };
      });
}
